plugin.tx_pxanewslettersubscription {
    settings {
        form {
            class = form-inline
            fields {
                outerWrap {
                    enabled = 0
                }
            }
            field {
                name {
                    outerWrap {
                        enabled = 0
                    }
                    innerWrap {
                        enabled = 1
                        class = form-group
                    }
                }
                email {
                    outerWrap {
                        enabled = 0
                    }
                    innerWrap {
                        enabled = 1
                        class = form-group
                    }
                }
            }
            button {
                subscribe {
                    outerWrap {
                        enabled = 1
                        class = subscribe-button form-actions
                    }
                    class = btn btn-default
                }
            }
        }
    }
}
