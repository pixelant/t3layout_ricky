# [t3layout](https://bitbucket.org/pixelant/t3layout_ricky) **0.1.0**
Extension with templates for **TYPO3**. Part of **Pixelant Core**.

[**Contributing**](#markdown-header-contributing)

[**Changelog**](#markdown-header-changelog)

***

###Before start work with t3layout, please read [documentation](https://bitbucket.org/pixelant/t3layout_ricky/wiki)

Documentation:

* [Pixelant Core](https://docs.google.com/a/pixelant.se/document/d/1gA5JR7k3WfwqxTjFcQdZu-EPBPTxWAdPe6pBC1PMH80/edit?usp=sharing)
* [t3layout](https://bitbucket.org/pixelant/t3layout_ricky/wiki/)
* [FElayout](https://bitbucket.org/pixelant/felayout_ricky/wiki)
* [pxa\_generic_content](https://bitbucket.org/pixelant/pxa_generic_content)


### Required dependencies for this repository:

* [TYPO3](http://typo3.org/) **Version >=6.2.0**
* [Fluid](http://fluidtypo3.org/) **Version >=6.2.0**
* [Flux](http://fluidtypo3.org/) **Version >=7.0.0**
* [VHS](http://fluidtypo3.org/) **Version >=2.0.0**

***

#### Git instructions

[Git workflow](https://docs.google.com/a/pixelant.se/document/d/1gVPXDCBrR66Ahhh7c6BMBeQZ0KpWWxhlLEw6zNOZX1g/edit?usp=sharing)

[Semantic Versioning](http://semver.org/)

***

# Contributing

Everyone can add fixes to this repository. Here is several rules for contribute this repository. Please keep this in mind for better cooperation.

### Issues

If you have a question not covered in the documentation or want to report a bug, the best way to ensure it gets addressed is to file it in the appropriate issues tracker.

* Used the search feature to ensure that the bug hasn't been reported before
* Try to reduce your code to the bare minimum required to reproduce the issue. This makes it much easier (and much faster) to isolate and fix the issue.


### Fixes

If you want to fix bug by yourself or add new features, you have to use steps listed below.

* Add your fixes:
    1. Clone repository `git clone git@bitbucket.org:pixelant/t3layout_ricky.git`
    2. Checkout to branch `dev` `git checkout -t origin/dev`
    3. Create new branch from `dev` `git checkout -b branchName`
    4. Add your changes, and commit it.
    5. Push your branch to repository `git push origin branchName`

* Recommend checking for issues, maybe somebody working with the same issue.
* Non-trivial changes should be discussed in an issue first.
* Dont forget about [editorconfig](http://editorconfig.org/).

>**Important!** Also it is possible to add changes directly to branch `dev` without pull request on Bitbucket. But main rule is to add changes to `dev` branch not `master`. Branch `master only for releases.

***

***
#Changelog
***

# **0.1.0** (18.09.2015)
* Added new BE layouts
* Updated news default detail template. Added constants to control disqus viewhelper
* Sets 'iconSet' path in pxa_generic_content to configured FEfolder
* Updated typoscript and removed template for pxa_newsletter_subscription settings
* Updated news templates
* Bug fixes

# **0.0.4** (22.08.2014)
* Beta release
* Reordered inclusions of some less files in ts for smallSite
* Fixed path of som less inclusions in ts for smallSite
* When smallSite is enabled, prefix is set in pxa_bootstrap
* Added detect IE function
* New Parallax
* Update submodule FElayout_ricky to v0.0.8

# **0.0.3** (08.08.2014)
* Added message for old browsers
* Modified variables
* Modified typoscript
* Added small project solution
* Added documentation
* Update submodule FElayout_ricky to v0.0.6

# **0.0.2** 
