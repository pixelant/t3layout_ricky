config {
    baseURL                 = {$config.baseURL}
    doctype                 = html5
    pageTitleFirst          = 1
    renderCharset           = utf-8
    metaCharset             = utf-8
    disablePrefixComment    = 1
    removeDefaultCss        = 1
    cache_period            = 43200
    index_enable            = 1
    index_externals         = 1
}

page = PAGE
page {
    typeNum = 0

    10 = FLUIDTEMPLATE
    10 {
        partialRootPath = EXT:{$page.t3layoutName}/Resources/Private/Partials/
        layoutRootPath = EXT:{$page.t3layoutName}/Resources/Private/Layouts/

        file.stdWrap.cObject = CASE
        file.stdWrap.cObject {
            key.data = levelfield:-1, backend_layout_next_level, slide
            key.override.field = backend_layout

            default = TEXT
            default.value = EXT:{$page.t3layoutName}/Resources/Private/Templates/Menu25%_Content75%.html
            default.insertData = 1

            t3layout_ricky__startpage100 = TEXT
            t3layout_ricky__startpage100.value = EXT:{$page.t3layoutName}/Resources/Private/Templates/StartPage100%.html
            t3layout_ricky__startpage100.insertData = 1

            t3layout_ricky__content100 = TEXT
            t3layout_ricky__content100.value = EXT:{$page.t3layoutName}/Resources/Private/Templates/Content100%.html
            t3layout_ricky__content100.insertData = 1

            t3layout_ricky__content25_50_menu25 = TEXT
            t3layout_ricky__content25_50_menu25.value = EXT:{$page.t3layoutName}/Resources/Private/Templates/Content25%_50%_Menu25%.html
            t3layout_ricky__content25_50_menu25.insertData = 1

            t3layout_ricky__content25_content75 = TEXT
            t3layout_ricky__content25_content75.value = EXT:{$page.t3layoutName}/Resources/Private/Templates/Content25%_Content75%.html
            t3layout_ricky__content25_content75.insertData = 1

            t3layout_ricky__content75_content25 = TEXT
            t3layout_ricky__content75_content25.value = EXT:{$page.t3layoutName}/Resources/Private/Templates/Content75%_Content25%.html
            t3layout_ricky__content75_content25.insertData = 1

            t3layout_ricky__content75_menu25 = TEXT
            t3layout_ricky__content75_menu25.value = EXT:{$page.t3layoutName}/Resources/Private/Templates/Content75%_Menu25%.html
            t3layout_ricky__content75_menu25.insertData = 1

            t3layout_ricky__menu25_content50_25 = TEXT
            t3layout_ricky__menu25_content50_25.value = EXT:{$page.t3layoutName}/Resources/Private/Templates/Menu25%_Content50%_25%.html
            t3layout_ricky__menu25_content50_25.insertData = 1

            t3layout_ricky__menu25_content75 = TEXT
            t3layout_ricky__menu25_content75.value = EXT:{$page.t3layoutName}/Resources/Private/Templates/Menu25%_Content75%.html
            t3layout_ricky__menu25_content75.insertData = 1

            t3layout_ricky__topcontent100_content100 = TEXT
            t3layout_ricky__topcontent100_content100.value = EXT:{$page.t3layoutName}/Resources/Private/Templates/TopContent100%_Content100%.html
            t3layout_ricky__topcontent100_content100.insertData = 1

            t3layout_ricky__topcontent100_content25_75 = TEXT
            t3layout_ricky__topcontent100_content25_75.value = EXT:{$page.t3layoutName}/Resources/Private/Templates/TopContent100%_Content25%_75%.html
            t3layout_ricky__topcontent100_content25_75.insertData = 1

            t3layout_ricky__topcontent100_content75_25 = TEXT
            t3layout_ricky__topcontent100_content75_25.value = EXT:{$page.t3layoutName}/Resources/Private/Templates/TopContent100%_Content75%_25%.html
            t3layout_ricky__topcontent100_content75_25.insertData = 1

            t3layout_ricky__blanktemplate = TEXT
            t3layout_ricky__blanktemplate.value = EXT:{$page.t3layoutName}/Resources/Private/Templates/BlankTemplate.html
            t3layout_ricky__blanktemplate.insertData = 1

        }
        variables {
            projectFolder = TEXT
            projectFolder.value = {$page.FEfolder}
            googleFontLink = TEXT
            googleFontLink.value = {$page.fontLink}
            faviconsMetaTags = TEXT
            faviconsMetaTags.value = {$page.faviconsMetaTags}
            # msTitleColor = TEXT
            # msTitleColor.value = {$page.msTitleColor}
            smallSite = TEXT
            smallSite.value = {$page.smallSite}
            # metaMenu < styles.content.get
            # metaMenu.select.where = colPos = 10
            # metaMenu.select.pidInList.data = leveluid:0
            # logo < styles.content.get
            # logo.select.where = colPos = 11
            # logo.select.pidInList.data = leveluid:0
            # icons < styles.content.get
            # icons.select.where = colPos = 12
            # icons.select.pidInList.data = leveluid:0
            footer < styles.content.get
            footer.select.where = colPos = 13
            footer.slide = -1
            header < styles.content.get
            header.select.where = colPos = 14
            header.slide = -1
            quicksearch < plugin.tx_pxasolr.quicksearch
        }
    }
}

page.meta.x-ua-compatible = ie=edge
page.meta.viewport = {$page.meta.viewport}

# Empty variables depending on constants
# [globalString = LIT:1 = {$tx_t3layout_ricky.config.disableSearchBox}]
#     page.10.variables.quicksearch >
# [global]

# Remove all js and put back those we need and in correct order, here f.ex we don't want pxa_bootstraps js.
page.includeJSFooterlibs >
[globalVar = LIT:1 = {$page.smallSite}]
    page.includeJSFooterlibs {
        t3layoutJs = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/main.js
        t3layoutCustomJs = {$page.FEfolder}/custom.js
        mocSearch = EXT:pxa_solr/Resources/Public/Scripts/moc_search.js
        handlebars = EXT:pxa_solr/Resources/Public/Scripts/handlebars.runtime-v1.3.0.js
        typeaheadJquery = EXT:pxa_solr/Resources/Public/Scripts/typeahead.jquery.min.js
        templates = EXT:pxa_solr/Resources/Public/Scripts/handlebars-templates.js
        typeahead = EXT:pxa_solr/Resources/Public/Scripts/typeahead.js
        pxa_newsletter_subscription = EXT:pxa_newsletter_subscription/Resources/Public/Js/script_jq.js
    }
[else]
    page.includeJSFooterlibs {
        t3layoutJs = {$page.FEfolder}/main.js
        mocSearch = EXT:pxa_solr/Resources/Public/Scripts/moc_search.js
        handlebars = EXT:pxa_solr/Resources/Public/Scripts/handlebars.runtime-v1.3.0.js
        typeaheadJquery = EXT:pxa_solr/Resources/Public/Scripts/typeahead.jquery.min.js
        templates = EXT:pxa_solr/Resources/Public/Scripts/handlebars-templates.js
        typeahead = EXT:pxa_solr/Resources/Public/Scripts/typeahead.js
        pxa_newsletter_subscription = EXT:pxa_newsletter_subscription/Resources/Public/Js/script_jq.js
    }
[end]


page.10.settings {
    # disableLanguageMenu = {$tx_t3layoutricky.config.disableLanguageMenu}
    disablePageHeaderOnTemplates = {$tx_t3layoutricky.config.disablePageHeaderOnTemplates}
    disableBreadcrumbsOnTemplates = {$tx_t3layoutricky.config.disableBreadcrumbsOnTemplates}
    defaultLanguageLabel = {$tx_t3layoutricky.config.defaultLanguageLabel}
    defaultIsoFlag = {$tx_t3layoutricky.config.defaultIsoFlag}
    hideNotTranslated = {$tx_t3layoutricky.config.hideNotTranslated}
}



<INCLUDE_TYPOSCRIPT: source="FILE:EXT:t3layout_ricky/Configuration/TypoScript/Includes/content.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:t3layout_ricky/Configuration/TypoScript/Includes/ext/felogin.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:t3layout_ricky/Configuration/TypoScript/Includes/ext/newsLetterSubscription.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:t3layout_ricky/Configuration/TypoScript/Includes/ext/pxa_solr.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:t3layout_ricky/Configuration/TypoScript/Includes/ext/news.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:t3layout_ricky/Configuration/TypoScript/Includes/ext/pxa_bootstrap.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:t3layout_ricky/Configuration/TypoScript/Includes/ext/pxa_generic_content.ts">
