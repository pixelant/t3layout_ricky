<?php

$EM_CONF[$_EXTKEY] = array(
	'title' => 't3layout_ricky',
	'description' => '',
	'category' => 'templates',
	'author' => '',
	'author_email' => '',
	'author_company' => 'Pixelant AB',
	'shy' => '',
	'priority' => 'top',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => '1',
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'version' => '0.1.0',
	'constraints' => array(
		'depends' => array(
			'typo3' => '6.2.0-6.2.99',
			'css_styled_content' => '6.2.0-6.2.99',
			'flux' => '',
			'vhs' => '',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);

?>
