plugin.tx_pxasolr {
    view {
        templateRootPaths {
            20 = EXT:{$page.t3layoutName}/Resources/Private/Extensions/pxa_solr/Templates
        }
        layoutRootPaths {
            20 = EXT:{$page.t3layoutName}/Resources/Private/Extensions/pxa_solr/Layouts
        }
        partialRootPaths {
            20 = EXT:{$page.t3layoutName}/Resources/Private/Extensions/pxa_solr/Partials
        }
    }
    settings {
        quicksearch {
            form {
                class = search
            }
        }
    }
}
