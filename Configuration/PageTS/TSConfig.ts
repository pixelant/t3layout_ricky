#### TCEFORM ####
TCEFORM {
    tt_content {
        layout {
            removeItems = 1,2,3
            disableNoMatchingValueElement = 1
            types {
                uploads {
                    removeItems = 3
                    altLabels {
                        0 = default
                        1 = icons
                        2 = icons-and-preview
                    }
                }
            }
        }
    }
}

#### RTE ####
RTE {
    default {
        contentCSS = typo3conf/ext/t3layout_ricky/Resources/Public/Css/rte.css
    }
}
