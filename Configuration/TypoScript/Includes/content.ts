###########################
####      IMAGES       ####
###########################
tt_content.image.20 {

    1 {
        imageLinkWrap {
            JSwindow = 0
            directImageLink = 1
        }
    }

    addClassesCol.ifEmpty =
    addClassesCol.override.cObject = COA
    addClassesCol.override.cObject {
        10 = CASE
        10 {
            key.field = imagecols

            default = TEXT
            default.value = col-sm-12

            2 < .default
            2.value = col-sm-6

            3 < .default
            3.value = col-sm-4 col-xs-6

            4 < .default
            4.value = col-sm-3 col-xs-6

            6 < .default
            6.value = col-sm-2 col-xs-6
        }
    }
    addClassesCol.override =
    addClassesCol.override.if {
        isGreaterThan.field = imagecols
        value = 1
    }
    addClassesImage >
    layout {
        default.override >
        default.value = <div class="image-center image-above###CLASSES###">###IMAGES######TEXT###</div>
        1.override >
        1.value = <div class="image-right image-above###CLASSES###">###IMAGES######TEXT###</div>
        2.override >
        2.value = <div class="image-left image-below###CLASSES###">###IMAGES######TEXT###</div>
        8.override >
        8.value = <div class="image-center image-below###CLASSES###">###TEXT######IMAGES###</div>
        9.override >
        9.value = <div class="image-right image-below###CLASSES###">###TEXT######IMAGES###</div>
        10.override >
        10.value = <div class="image-left image-below###CLASSES###">###TEXT######IMAGES###</div>
        17.override >
        17.value = <div class="image-intext-right image-intext###CLASSES###">###IMAGES######TEXT###</div>
        18.override >
        18.value = <div class="image-intext-left image-intext###CLASSES###">###IMAGES######TEXT###</div>
        25.value = <div class="image-beside-right image-beside###CLASSES###">###IMAGES######TEXT###</div>
        25.override >
        25.override = <div class="image-header-{field:header_layout} image-beside-right image-beside###CLASSES###">###IMAGES######TEXT###</div>
        25.override.if.isTrue.field = header
        25.override.insertData = 1
        26.value = <div class="image-beside-left image-beside###CLASSES###">###IMAGES######TEXT###</div>
        26.override >
        26.override = <div class="image-header-{field:header_layout} image-beside-left image-beside###CLASSES###">###IMAGES######TEXT###</div>
        26.override.if.isTrue.field = header
        26.override.insertData = 1
    }
    rendering {
        singleNoCaption {
            allStdWrap {
                dataWrap = <div class="image-wrap"> | </div>
                dataWrap {
                    override = <div class="image-wrap"> | </div>
                }
                innerWrap.cObject.0.value = <div class="image-center-outer"><div class="image-center-inner"> | </div></div>
                innerWrap.cObject.8.value = <div class="image-center-outer"><div class="image-center-inner"> | </div></div>
            }
            singleStdWrap {
                wrap = <div class="image###CLASSES###"> | </div>
                wrap {
                    override.cObject = COA
                    override.cObject {
                        10 = TEXT
                        10 {
                            value = <figure class="image###CLASSES###">|</figure>
                            override = <figure class="thumbnail###CLASSES###">|</figure>
                            override {
                                if {
                                    value = 1
                                    equals.field = imageborder
                                }
                            }
                        }
                    }
                }
            }
        }
        noCaption {
            allStdWrap {
                dataWrap = <div class="image-wrap"> | </div>
                dataWrap {
                    override = <div class="image-wrap"> | </div>
                }
            }
            singleStdWrap {
                wrap = <div class="image###CLASSES###"> | </div>
                wrap {
                    override.cObject = COA
                    override.cObject {
                        10 = TEXT
                        10 {
                            value = <figure class="image###CLASSES###">|</figure>
                            override = <figure class="thumbnail###CLASSES###">|</figure>
                            override {
                                if {
                                    value = 1
                                    equals.field = imageborder
                                }
                            }
                        }
                    }
                }
            }
            rowStdWrap.wrap = <div class="image-row"> | </div>
            noRowsStdWrap.wrap = <div class="image-row"> | </div>
            lastRowStdWrap.wrap = <div class="image-row"> | </div>
            columnStdWrap.wrap = <div class="image-column###CLASSES###"> | </div>
        }
        singleCaption {
            singleStdWrap {
                wrap {
                    override.cObject = COA
                    override.cObject {
                        10 = TEXT
                        10 {
                            value = <figure class="image###CLASSES###">|###CAPTION###</figure>
                            override = <figure class="thumbnail###CLASSES###">|###CAPTION###</figure>
                            override {
                                if {
                                    value = 1
                                    equals.field = imageborder
                                }
                            }
                        }
                    }
                }
            }
            caption {
                wrap = <caption class="caption###CLASSES###"> | </caption>
                wrap.override = <figcaption class="caption###CLASSES###"> | </figcaption>
            }
        }
        splitCaption {
            singleStdWrap {
                wrap {
                    override.cObject = COA
                    override.cObject {
                        10 = TEXT
                        10 {
                            value = <figure class="image###CLASSES###">|###CAPTION###</figure>
                            override = <figure class="thumbnail###CLASSES###">|###CAPTION###</figure>
                            override {
                                if {
                                    value = 1
                                    equals.field = imageborder
                                }
                            }
                        }
                    }
                }
            }
            caption {
                wrap = <caption class="caption###CLASSES###"> | </caption>
                wrap.override = <figcaption class="caption###CLASSES###"> | </figcaption>
            }
            rowStdWrap.wrap = <div class="image-row"> | </div>
            noRowsStdWrap.wrap = <div class="image-row"> | </div>
            lastRowStdWrap.wrap = <div class="image-row"> | </div>
            columnStdWrap.wrap = <div class="image-column###CLASSES###"> | </div>
        }
        globalCaption {
            allStdWrap {
                dataWrap = <div class="image-wrap"> | ###CAPTION###</div>
                dataWrap {
                    override = <figure class="image-wrap"> | ###CAPTION###</figure>
                }
            }
            singleStdWrap {
                wrap = <div class="image###CLASSES###"> | </div>
                wrap {
                    override.cObject = COA
                    override.cObject {
                        10 = TEXT
                        10 {
                            value = <div class="image###CLASSES###">|</div>
                            override = <div class="thumbnail###CLASSES###">|</div>
                            override {
                                if {
                                    value = 1
                                    equals.field = imageborder
                                }
                            }
                        }
                    }
                }
            }
            caption {
                wrap = <caption class="caption"> | </caption>
                wrap.override = <figcaption class="caption###CLASSES###"> | </figcaption>
            }
            rowStdWrap.wrap = <div class="image-row"> | </div>
            noRowsStdWrap.wrap = <div class="image-row"> | </div>
            lastRowStdWrap.wrap = <div class="image-row"> | </div>
            columnStdWrap.wrap = <div class="image-column###CLASSES###"> | </div>
        }
    }
}

########################
#### CTYPE: TEXTPIC ####
########################
tt_content.textpic.20 {
    text.wrap = <div class="text"> | </div>
    text.10.10.stdWrap.dataWrap = |
}


########################
#### CTYPE: UPLOADS ####
########################
tt_content.uploads.20 {
    renderObj >
    renderObj = COA
    renderObj {
        10 = IMAGE
        10 {
            file.import.data = file:current:originalUid // file:current:uid
            file.width = 100
            stdWrap {
                if.value = 1
                if.isGreaterThan.field = layout
                typolink {
                    parameter.data = file:current:originalUid // file:current:uid
                    parameter.wrap = file:|
                    fileTarget < lib.parseTarget
                    fileTarget =
                    fileTarget.override = {$styles.content.uploads.target}
                    fileTarget.override.override.field = target
                    removePrependedNumbers = 1
                    ATagParams = class="pull-left"
                }
            }
        }
        20 = COA
        20 {
            10 = COA
            10 {
                10 = COA
                10 {
                    10 = CASE
                    10 {
                        key.data = file:current:extension
                        key.case = lower
                        default = TEXT
                        default.value = icons icon-t3-file
                        default.stdWrap.noTrimWrap = |<span class="|"></span> |
                        avi  =< tt_content.uploads.20.renderObj.20.10.10.10.default
                        avi.value = icons icon-t3-film
                        mov  =< tt_content.uploads.20.renderObj.20.10.10.10.avi
                        mpg  =< tt_content.uploads.20.renderObj.20.10.10.10.avi
                        mpeg =< tt_content.uploads.20.renderObj.20.10.10.10.avi
                        mkv  =< tt_content.uploads.20.renderObj.20.10.10.10.avi
                        jpg  =< tt_content.uploads.20.renderObj.20.10.10.10.default
                        jpg.value = icons icon-t3-picture
                        gif  =< tt_content.uploads.20.renderObj.20.10.10.10.jpg
                        png  =< tt_content.uploads.20.renderObj.20.10.10.10.jpg
                        bmp  =< tt_content.uploads.20.renderObj.20.10.10.10.jpg
                        ai   =< tt_content.uploads.20.renderObj.20.10.10.10.jpg
                        eps  =< tt_content.uploads.20.renderObj.20.10.10.10.jpg
                        ico  =< tt_content.uploads.20.renderObj.20.10.10.10.jpg
                        tga  =< tt_content.uploads.20.renderObj.20.10.10.10.jpg
                        tif  =< tt_content.uploads.20.renderObj.20.10.10.10.jpg
                        if {
                            value = 0
                            isGreaterThan.field = layout
                        }
                    }
                    20 = TEXT
                    20 {
                        data = file:current:name
                        override.data = file:current:title
                        htmlSpecialChars = 1
                        required = 1
                        replacement {
                            10 {
                                search = _
                                replace.char = 32
                            }
                        }
                    }
                    stdWrap.typolink < tt_content.uploads.20.renderObj.10.stdWrap.typolink
                    stdWrap.typolink.ATagParams >
                }
                40 = TEXT
                40 {
                    if.isTrue.field = filelink_size
                    data = file:current:size
                    noTrimWrap = | <small class="text-muted">|</small>|
                    bytes = 1
                    bytes.labels = {$styles.content.uploads.filesizeBytesLabels}
                }
                stdWrap.wrap = <h4 class="media-heading">|</h4>
            }
            30 = TEXT
            30 {
                data = file:current:description
                htmlSpecialChars = 1
                wrap = |
                required = 1
            }
            wrap = <div class="media-body">|</div>
        }
        wrap = <li class="media">|</li>
    }
    stdWrap {
        dataWrap = <ul class="media-list">|</ul>
    }
}

#########################
#### CTYPE: MAILFORM ####
#########################
plugin.tx_form._CSS_DEFAULT_STYLE >

tt_content.mailform.20 {
    layout {
        form (
            <form class="form-horizontal mail-form-t3" role="form">
                <containerWrap />
            </form>
        )
        containerWrap (
            <div>
                <elements />
            </div>
        )
        elementWrap (
            <div>
                <element />
            </div>
        )
        label (
            <label>
                <labelvalue />
                <mandatory />
            </label>
        )
        error (
            <p class="text-danger">
                <errorvalue />
            </p>
        )
        textline (
            <div class="form-group">
                <div class="col-sm-3 control-label" >
                <label />
                </div>
                <div class="col-sm-9">
                    <input class="form-control" />
                    <error />
                </div>
            </div>
        )
        textarea (
            <div class="form-group">
                <div class="col-sm-3 control-label">
                    <label />
                </div>
                <div class="col-sm-9">
                    <textarea class="form-control" />
                    <error />
                </div>
            </div>
        )
        submit (
            <div class="form-group mail-submit ">
                <div class="col-sm-offset-3 col-sm-9">
                    <input class="btn btn-default" />
                </div>
            </div>
        )
        reset (
            <div class="form-group mail-reset">
                <div class="col-sm-offset-3 col-sm-9">
                    <input class="btn btn-default" />
                </div>
            </div>
        )
        checkbox (
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <div class="checkbox">
                    <div class="fix-label">
                        <input />
                        <label />
                    </div>
                </div>
                <error />
            </div>
        </div>
        )
        radio (
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <div class="radio">
                    <div class="fix-label">
                        <input />
                        <label />
                    </div>
                </div>
                <error />
            </div>
        </div>
        )
        select (
        <div class="form-group">
            <div class="col-sm-3 control-label" >
                <label/>
            </div>
            <div class="col-sm-9">
                <select class="form-control">
                    <elements />
                </select>
                <error />
            </div>
        </div>
        )
        mandatory (
            <span class="icons icon-t3-mandatory">
                <mandatoryvalue />
            </span>
        )
        fieldset (
            <fieldset><legend /><containerWrap /></fieldset>
        )
    }
    stdWrap.wrap = |
}
