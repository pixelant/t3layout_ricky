<?php
namespace Pixelant\t3layout_ricky\Hooks;

use \TYPO3\CMS\Backend\View\BackendLayout\BackendLayout;
use \TYPO3\CMS\Backend\View\BackendLayout\DataProviderContext;
use \TYPO3\CMS\Backend\View\BackendLayout\BackendLayoutCollection;
use \TYPO3\CMS\Backend\Utility\BackendUtility;


class BackendLayoutDataProvider implements \TYPO3\CMS\Backend\View\BackendLayout\DataProviderInterface {

    protected $backendLayouts = array(
        'startpage100' => array(
            'title' => 't3layout_ricky: Start Page(100%)',
            'config' => '
                backend_layout {
                    colCount = 6
                    rowCount = 3
                    rows {
                        1 {
                            columns {
                                1 {
                                    name = Header
                                    colPos = 14
                                    colspan = 6
                                }
                            }
                        }
                        2 {
                            columns {
                                1 {
                                    name = Main Content
                                    colPos = 0
                                    colspan = 6
                                }
                            }
                        }
                        3 {
                            columns {
                                1 {
                                    name = Footer
                                    colPos = 13
                                    colspan = 6
                                }
                            }
                        }
                    }
                }
            ',
            'icon' => 'EXT:t3layout_ricky/Resources/Public/Images/BackendLayouts/layout_1.png'
        ),

        'content100' => array(
            'title' => 't3layout_ricky: Content_1column(100%)',
            'config' => '
                backend_layout {
                    colCount = 6
                    rowCount = 1
                    rows {
                        1 {
                            columns {
                                1 {
                                    name = Content
                                    colPos = 0
                                    colspan = 6
                                }
                            }
                        }
                    }
                }
            ',
            'icon' => 'EXT:t3layout_ricky/Resources/Public/Images/BackendLayouts/layout_8.png'
        ),

        'content25_50_menu25' => array(
            'title' => 't3layout_ricky: Content_2columns(25%_50%) + Menu(25%)',
            'config' => '
                backend_layout {
                    colCount = 6
                    rowCount = 1
                    rows {
                        1 {
                            columns {
                                1 {
                                    name = Left Content
                                    colPos = 1
                                    colspan = 1
                                }
                                2 {
                                    name = Main Content
                                    colPos = 0
                                    colspan = 4
                                }
                                3 {
                                    name = Subnavigation
                                    colspan = 1
                                }
                            }
                        }
                    }
                }
            ',
            'icon' => 'EXT:t3layout_ricky/Resources/Public/Images/BackendLayouts/layout_7.png'
        ),

        'content25_content75' => array(
            'title' => 't3layout_ricky: Content_2columns(25%_75%)',
            'config' => '
                backend_layout {
                    colCount = 6
                    rowCount = 1
                    rows {
                        1 {
                            columns {
                                1 {
                                    name = Left Content
                                    colPos = 1
                                    colspan = 2
                                }
                                2 {
                                    name = Content
                                    colPos = 0
                                    colspan = 4
                                }
                            }
                        }
                    }
                }
            ',
            'icon' => 'EXT:t3layout_ricky/Resources/Public/Images/BackendLayouts/layout_4.png'
        ),

        'content75_content25' => array(
            'title' => 't3layout_ricky: Content_2columns(75%_25%)',
            'config' => '
                backend_layout {
                    colCount = 6
                    rowCount = 1
                    rows {
                        1 {
                            columns {
                                1 {
                                    name = Content
                                    colPos = 0
                                    colspan = 2
                                }
                                2 {
                                    name = Right Content
                                    colPos = 1
                                    colspan = 4
                                }
                            }
                        }
                    }
                }
            ',
            'icon' => 'EXT:t3layout_ricky/Resources/Public/Images/BackendLayouts/layout_5.png'
        ),

        'content75_menu25' => array(
            'title' => 't3layout_ricky: Content_1column(75%) + Menu(25%)',
            'config' => '
                backend_layout {
                    colCount = 6
                    rowCount = 1
                    rows {
                        1 {
                            columns {
                                1 {
                                    name = Main Content
                                    colPos = 0
                                    colspan = 5
                                }
                                2 {
                                    name = Subnavigation
                                    colspan = 1
                                }
                            }
                        }
                    }
                }
            ',
            'icon' => 'EXT:t3layout_ricky/Resources/Public/Images/BackendLayouts/layout_3.png'
        ),

        'menu25_content50_25' => array(
            'title' => 't3layout_ricky: Menu(25%) + Content_2columns(50%_25%)',
            'config' => '
                backend_layout {
                    colCount = 6
                    rowCount = 1
                    rows {
                        1 {
                            columns {
                                1 {
                                    name = Subnavigation
                                    colspan = 1
                                }
                                2 {
                                    name = Main Content
                                    colPos = 0
                                    colspan = 4
                                }
                                3 {
                                    name = Right Content
                                    colPos = 1
                                    colspan = 1
                                }
                            }
                        }
                    }
                }
            ',
            'icon' => 'EXT:t3layout_ricky/Resources/Public/Images/BackendLayouts/layout_6.png'
        ),

        'menu25_content75' => array(
            'title' => 't3layout_ricky: Menu(25%) + Content_1column(75%)',
            'config' => '
                backend_layout {
                    colCount = 6
                    rowCount = 1
                    rows {
                        1 {
                            columns {
                                1 {
                                    name = Subnavigation
                                    colspan = 1
                                }
                                2 {
                                    name = Main Content
                                    colPos = 0
                                    colspan = 5
                                }
                            }
                        }
                    }
                }
            ',
            'icon' => 'EXT:t3layout_ricky/Resources/Public/Images/BackendLayouts/layout_2.png'
        ),

        'topcontent100_content100' => array(
            'title' => 't3layout_ricky: TopContent(100%) + Content_1column(100%)',
            'config' => '
                backend_layout {
                    colCount = 6
                    rowCount = 2
                    rows {
                        1 {
                            columns {
                                1 {
                                    name = TopContent
                                    colPos = 3
                                    colspan = 6
                                }
                            }
                        }
                        2 {
                            columns {
                                1 {
                                    name = Content
                                    colPos = 0
                                    colspan = 6
                                }
                            }
                        }
                    }
                }
            ',
            'icon' => 'EXT:t3layout_ricky/Resources/Public/Images/BackendLayouts/layout_11.png'
        ),

        'topcontent100_content25_75' => array(
            'title' => 't3layout_ricky: TopContent(100%) + Content_2columns(25%_75%)',
            'config' => '
                backend_layout {
                    colCount = 6
                    rowCount = 2
                    rows {
                        1 {
                            columns {
                                1 {
                                    name = TopContent
                                    colPos = 3
                                    colspan = 6
                                }
                            }
                        }
                        2 {
                            columns {
                                1 {
                                    name = Left Content
                                    colPos = 1
                                    colspan = 2
                                }
                                2 {
                                    name = Content
                                    colPos = 0
                                    colspan = 4
                                }
                            }
                        }
                    }
                }
            ',
            'icon' => 'EXT:t3layout_ricky/Resources/Public/Images/BackendLayouts/layout_10.png'
        ),

        'topcontent100_content75_25' => array(
            'title' => 't3layout_ricky: TopContent(100%) + Content_2columns(75%_25%)',
            'config' => '
                backend_layout {
                    colCount = 6
                    rowCount = 2
                    rows {
                        1 {
                            columns {
                                1 {
                                    name = TopContent
                                    colPos = 3
                                    colspan = 6
                                }
                            }
                        }
                        2 {
                            columns {
                                1 {
                                    name = Content
                                    colPos = 0
                                    colspan = 4
                                }
                                2 {
                                    name = Right Content
                                    colPos = 1
                                    colspan = 2
                                }
                            }
                        }
                    }
                }
            ',
            'icon' => 'EXT:t3layout_ricky/Resources/Public/Images/BackendLayouts/layout_9.png'
        ),

        'blanktemplate' => array(
            'title' => 't3layout_ricky: Blank Template',
            'config' => '
                backend_layout {
                    colCount = 6
                    rowCount = 1
                    rows {
                        1 {
                            columns {
                                1 {
                                    name = Content
                                    colPos = 0
                                    colspan = 6
                                }
                            }
                        }
                    }
                }
            ',
        )

    );

    /**
     * @param DataProviderContext $dataProviderContext
     * @param BackendLayoutCollection $backendLayoutCollection
     * @return void
     */
    public function addBackendLayouts(DataProviderContext $dataProviderContext, BackendLayoutCollection $backendLayoutCollection) {
        foreach ($this->backendLayouts as $key => $data) {
            $data['uid'] = $key;
            $backendLayout = $this->createBackendLayout($data);
            $backendLayoutCollection->add($backendLayout);
        }
    }

    /**
     * Gets a backend layout by (regular) identifier.
     *
     * @param string $identifier
     * @param integer $pageId
     * @return NULL|BackendLayout
     */
    public function getBackendLayout($identifier, $pageId){
        $backendLayout = NULL;
        if(array_key_exists($identifier,$this->backendLayouts)) {
            return $this->createBackendLayout($this->backendLayouts[$identifier]);
        }
        return $backendLayout;
    }

    /**
     * Creates a new backend layout using the given record data.
     *
     * @param array $data
     * @return BackendLayout
     */
    protected function createBackendLayout(array $data) {
        $backendLayout = BackendLayout::create($data['uid'], $data['title'], $data['config']);
        $backendLayout->setIconPath($this->getIconPath($data['icon']));
        $backendLayout->setData($data);
        return $backendLayout;
    }

    /**
     * Gets and sanitizes the icon path.
     *
     * @param string $icon Name of the icon file
     * @return string
     */
    protected function getIconPath($icon) {
        $iconPath = '';
        if (!empty($icon)) {
            $iconPath = $icon;
        }
        return $iconPath;
    }

}
