<?php
if(!defined('TYPO3_MODE')){
    die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', $_EXTKEY);

/***************
 * Add custom templateLayouts for news extension flexforms.
 */
$GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['templateLayouts']['list_1'] = array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xlf:news.templateLayouts.list_1', 'List/List-1');
$GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['templateLayouts']['detail_1'] = array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xlf:news.templateLayouts.detail_1', 'Detail/Detail-1');
$GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['templateLayouts']['newsCarousel'] = array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xlf:news.templateLayouts.newsCarousel', 'List/newsCarousel');

/***************
 * BackendLayoutDataProvider
 */
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['BackendLayoutDataProvider'][$_EXTKEY] = 'Pixelant\\' . $_EXTKEY . '\Hooks\BackendLayoutDataProvider';


