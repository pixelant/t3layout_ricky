# *********************
# first remove default less and js files from pxa_bootstrap
# they are included in file: pxa_bootstrap/Configuration/TypoScript/Includes/Setup/page.ts
# so check in it as a reference.
# *********************

# clear all less inlcusions
plugin.tx_pxabootstrap.settings.less.includes >

# clear all js inlcusions
page.includeJSFooterlibs {
    bootstrapTransition >
    bootstrapAlert >
    bootstrapButton >
    bootstrapCarousel >
    bootstrapCollapse >
    bootstrapDropDown >
    bootstrapModal >
    bootstrapTooltip >
    bootstrapPopover >
    bootstrapScrollspy >
    bootstrapTab >
    bootstrapAffix >
}

# Make sure the t3contentBs3.css doensn't get outputted? (has stuff for aligning headers and figures, figure-captions)
page.includeCSS.t3contentBs3 >

# *********************
# in pxa_bootstrap all bootstrap less files are included "manually" instead of using the bootstrap.less so we can add f.ex. out custom.less right after variables.less
# *********************



[globalVar = LIT:1 = {$page.smallSite}]
plugin.tx_pxabootstrap.settings.less.disabled = 0
plugin.tx_pxabootstrap.settings.less.cssUrlPrefix.enabled = 1
plugin.tx_pxabootstrap.settings.less.cssUrlPrefix.prefix = {$page.FEfolder}
[else]
plugin.tx_pxabootstrap.settings.less.disabled = 1
[end]



# Configure "less" part of pxa_bootstrap again.
plugin.tx_pxabootstrap {
    settings {
        less {
            includes {
                variables {
                        # Try to only place less files here with variables and mixins
                        # The "section" variables is always included when trying to parse less files in debug mode.
                        # Less files in other "sections" are expected to output some css after parsing it together with variables and mixins.
                        # Do not add comments in variable files, then it will be outputted when parsing less files and won't "detect" empty files.
                    10 = {$page.FEfolder}/customVariables.less
                    20 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/variables.less
                    30 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/alerts.less
                    40 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/background-variant.less
                    50 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/border-radius.less
                    60 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/buttons.less
                    70 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/center-block.less
                    80 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/clearfix.less
                    90 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/forms.less
                    100 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/gradients.less
                    110 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/grid-framework.less
                    120 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/grid.less
                    130 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/hide-text.less
                    140 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/image.less
                    150 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/labels.less
                    160 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/list-group.less
                    170 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/nav-divider.less
                    180 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/nav-vertical-align.less
                    190 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/opacity.less
                    200 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/pagination.less
                    210 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/panels.less
                    220 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/progress-bar.less
                    230 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/reset-filter.less
                    240 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/resize.less
                    250 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/responsive-visibility.less
                    260 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/size.less
                    270 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/tab-focus.less
                    280 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/table-row.less
                    290 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/text-emphasis.less
                    300 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/text-overflow.less
                    310 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/mixins/vendor-prefixes.less

                        # icons, moved here because other less files depends on it
                    320 = {$page.FEfolder}/icons.less

                        # Moved here so it is prior to other bootstrap less files.
                    330 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/normalize.less
                    340 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/print.less

                        # Moved here from bootstrap section because ohther less files depends on it
                    350 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/utilities.less
                    360 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/forms.less
                }
                bootstrap {

                    30 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/scaffolding.less
                    40 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/type.less
                    50 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/code.less
                    60 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/grid.less
                    70 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/tables.less
                    90 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/buttons.less

                    100 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/component-animations.less
                    # 110 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/glyphicons.less
                    120 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/dropdowns.less
                    130 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/button-groups.less
                    140 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/input-groups.less
                    150 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/navs.less
                    160 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/navbar.less
                    170 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/breadcrumbs.less
                    180 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/pagination.less
                    190 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/pager.less
                    200 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/labels.less
                    210 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/badges.less
                    220 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/jumbotron.less
                    230 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/thumbnails.less
                    240 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/alerts.less
                    250 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/progress-bars.less
                    260 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/media.less
                    270 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/list-group.less
                    280 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/panels.less
                    290 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/wells.less
                    300 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/close.less

                    310 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/modals.less
                    # 320 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/tooltip.less
                    # 330 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/popovers.less
                    340 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/carousel.less

                    360 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/bootstrap/responsive-utilities.less
                }

                components {
                    10 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/base/components.css

                }
                maincss {

                    // HEADER
                    30 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/header/header.less

                    // FOOTER
                    40 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/footer/footer.less

                    // subNavigation
                    50 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/subNavigation/subNavigation.less

                    // Breadcrumb
                    60 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/breadcrumb/breadcrumb.less

                    // Ext
                    110 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/ext/felogin.less
                    115 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/ext/newsLetterSubscription.less
                    120 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/ext/isotope.less
                    // ext News
                    125 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/ext/news/detail.less
                    130 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/ext/news/list.less
                    135 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/ext/news/newsCarousel.less
                    140 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/ext/news/news.less
                    // ext Search
                    145 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/ext/search/searchResult.less


                    // regularContentElements
                    150 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/regularContentElements/regularContentElements.less
                    155 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/regularContentElements/divider.less
                    160 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/regularContentElements/mailForm.less

                    // generalContentElements
                    165 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/genericContentElements/1_BigIconTextButton.less
                    170 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/genericContentElements/2_ImageTextLink.less
                    175 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/genericContentElements/3_LeftIconTextButton.less
                    180 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/genericContentElements/4_PageHeader.less
                    185 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/genericContentElements/quote.less
                    190 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/genericContentElements/logoCarousel.less
                    195 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/genericContentElements/accordion.less
                    200 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/genericContentElements/carousel.less
                    205 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/genericContentElements/logo.less
                    210 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/genericContentElements/social.less
                    215 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/genericContentElements/tabs.less
                    220 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/genericContentElements/parallax.less
                    225 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/genericContentElements/languageMenu.less
                    230 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/genericContentElements/mainNavigation.less
                    235 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/genericContentElements/extendedMainNav.less
                    240 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/genericContentElements/mainNavToggleButton.less
                    245 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/genericContentElements/searchForm.less
                    250 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/genericContentElements/searchToggleButton.less

                    // print styles
                    300 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/print.less
                    // special styles for layouts
                    310 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/layouts.less

                    // General styles. When you can't find where to place css, or want to test something.
                    330 = EXT:{$page.t3layoutName}/Resources/Public/felayout_ricky/dev/styles/general.less

                }
                custom{
                    20 = {$page.FEfolder}/custom.less
                }
            }
        }
    }
}


