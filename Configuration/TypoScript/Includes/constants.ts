config {
    # cat=t3layout: //110; type=string; label=BaseURL:
    baseURL                         =
}
page.meta {
    # cat=t3layout: //170; type=string; label=Vieport:
    viewport                        = width=device-width, initial-scale=1.0, user-scalable=no
}

page {
        # cat=t3layout: //180; type=string; label=Project folder:
        FEfolder                        = fileadmin/felayout_ricky
        # cat=t3layout: //180; type=string; label=Layout name:
        t3layoutName                    = t3layout_ricky
        # cat=t3layout: //190; type=string; label=Google font link:
        fontLink                        = //fonts.googleapis.com/css?family=Open+Sans:400
        # cat=t3layout: //192; type=string; label=Ms Title Color:
        #msTitleColor                    = #00a300
        # cat=t3layout: //192; type=boolean; label=Small site:
        smallSite                       = 0
        # cat=t3layout: //193; type=string; label=Favicons meta tags source:
        faviconsMetaTags                    = fileadmin/felayout_ricky/partials/faviconsMetaTags.html
}

tx_t3layoutricky.config {
    # # cat=t3layout: //200; type=boolean; label=Disable Quicksearch
    # disableSearchBox                    = 0

    # # cat=t3layout: //201; type=boolean; label=Language menu: Disable
    # disableLanguageMenu                 = 0

    # cat=t3layout: //202; type=boolean; label=PageHeader On Templates: Disable
    disablePageHeaderOnTemplates        = 0

    # cat=t3layout: //203; type=boolean; label=Breadcrumbs On Templates: Disable
    disableBreadcrumbsOnTemplates       = 0

    # cat=t3layout: //250; type=string; label=Language menu: Default Language Label
    defaultLanguageLabel                = English

    # cat=t3layout: //251; type=string; label=Language menu: Default Iso Flag
    defaultIsoFlag                      = gb

    # cat=t3layout: //252; type=boolean; label=Language menu: Hide Non Translated
    hideNotTranslated                   = 1

    # cat=t3layout: //260; type=string; label=Disqus shortname (Enables disqus comments in single news if available in selected template)
    disqusShortname =

    # cat=t3layout: //265; type=boolean; label=Enables social share buttons in news (if available in selected template)
    showSocialShareButtons = 0
}

tx_pxabootstrap.config {
    includeTYPO3ContribJquery = 0
    includeBootstrapJsInFooterlibs = 0
}
