plugin.tx_news {
    view {
        templateRootPaths {
            20 = EXT:{$page.t3layoutName}/Resources/Private/Extensions/news/Templates/
        }
        layoutRootPaths {
            20 = EXT:news/Resources/Private/Layouts/
        }
        partialRootPaths {
            20 = EXT:{$page.t3layoutName}/Resources/Private/Extensions/news/Partials/
        }
    }

    settings {
        detail {
            disqusShortname = {$tx_t3layoutricky.config.disqusShortname}
            showSocialShareButtons = {$tx_t3layoutricky.config.showSocialShareButtons}
        }
    }
}



plugin.tx_news.settings.cssFile >
