
# Set folder where icon font is present (including css and json files)
plugin.tx_pxagenericcontent {
  settings {
    iconSet {
      sourceFolder = {$page.FEfolder}/fonts
    }
  }
}
